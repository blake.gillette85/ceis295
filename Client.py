#Name:  Bobby Estey
#Date:  20210815

class Client:
    def __init__(self, client_id=0, first_name="Unknown", last_name="Unknown", phone="Unknown", email="Unknown"):
        self.__client_id = client_id
        self.__first_name = first_name
        self.__last_name = last_name
        self.__phone = phone
        self.__email = email
        
    # classes that compare objects must implement __eq__ and __lt__ method
    # __lt__ means less than and must return a boolean
    # __le__ means less than or equal to and must return a boolean
    # __eq__ means equal to and must return a boolean
    def __lt__(self, other):
        this_full_name = self.__last_name + " " + self.__first_name
        other_full_name = other.__last_name + " " + other.__first_name
        return this_full_name < other_full_name
    
    def __le__(self, other):
        this_full_name = self.__last_name + " " + self.__first_name
        other_full_name = other.__last_name + " " + other.__first_name
        return this_full_name <= other_full_name
    
    def __eq__(self, other):
        return self.__client_id == other.__client_id
    
    # __str__ auto called when you print the object
    def __str__(self):
        return str(self.__client_id) + ", " + self.__last_name + ", " + self.__first_name
          
    # getters and setters
    # will do validation typically per industry standards
    def get_client_id(self):
        return self.__client_id
    
    def set_client_id(self, client_id):
        self.__client_id = client_id
        
    def get_first_name(self):
        return self.__first_name
    
    def set_first_name(self, first_name):
        self.__first_name = first_name
    
    def get_last_name(self):
        return self.__last_name
    
    def set_last_name(self, last_name):
        self.__last_name = last_name
        
    def get_phone(self):
        return self.__phone
    
    def set_phone(self, phone):
        self.__phone = phone
        
    def get_email(self):
        return self.__email
    
    def set_email(self, email):
        self.__email = email
        